#Description#

This machine ethics software library is aimed at supporting the programming of hybrid ethical reasoning agents (HERA), that is, agents that are aware of multiple ethical principles. These kinds of agents are able to judge moral cases from various ethical standpoints and to come up with nuanced judgements.

#How to Use It#


##Representational Format##

TODO

##Application of Principles to a Case##

TODO