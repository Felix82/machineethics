from semantics import CausalModel
from language import Must, Or, PCauses, Causes, And, K, Not, May, Eq, U, Explains, Intervention


model = CausalModel("./cases2/lying-robot.json", {"lying_0": 1, "asking_0": 0, "refraining_0": 0, "lying_1": 0, "asking_1": 1, "refraining_1": 0})
print(model)

f0 = Or("party_0", "party_1")
print(f0, model.models(f0))

f1 = Causes("asking_1", "party_1")
print(f1, model.models(f1))

f2 = Causes("lying_0", "party_1")
print(f2, model.models(f2))

model = CausalModel("./cases2/lying-robot.json", {"lying_0": 0, "asking_0": 1, "refraining_0": 0, "lying_1": 1, "asking_1": 0, "refraining_1": 0})
print(model) 

f0 = Or("party_0", "party_1")
print(f0, model.models(f0))

actions = ["lying", "asking", "refraining"]
num_steps = 2

def getAllPossiblePlans(actions, num_steps, result):
    if num_steps == 0:
        return result
    else:
        if len(result) == 0:
            for i in actions:
                result.append([i])
            return getAllPossiblePlans(actions, num_steps - 1, result)
        else:
            result_new = []
            for r in result:
                for i in actions:
                    result_new.append(r+[i])
            return getAllPossiblePlans(actions, num_steps - 1, result_new)


def planStep(model_file, actions, num_steps, goal):
    plan_candidates = getAllPossiblePlans(actions, num_steps, [])
    print(num_steps)
    print(plan_candidates)
    print("*"*50)
    for p in plan_candidates:
        world = {}
        for a in actions:
            for i in range(num_steps):
                if p[i] == a:
                    world[a+"_"+str(i)] = 1
                else:
                    world[a+"_"+str(i)] = 0
        
        model = CausalModel(model_file, world)
        if model.models(goal):
            return p
            
def plan(model_file, actions, horizon, goal):
    for i in range(horizon):
        plan = planStep(model_file, actions, i+1, goal)
        if plan is not None:
            return plan
            
            
goal = "party_1"
plan = plan("./cases2/lying-robot.json", actions, 2, goal)
print(plan)

