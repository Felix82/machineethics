from principles import DoubleEffectPrinciple, UtilitarianPrinciple, DoNoHarmPrinciple, ParetoPrinciple
from semantics import CausalModel, ActionTypeModel, World
from language import Must, Or, Causes, And, Prevents, Not

"""
The Causal Network: Represents what the HERA knows
about actions and their consequences.
"""
m = CausalModel("./cases/fatman-trolley-problem.json")

"""
Possible Worlds representing
the situation the HERA can
encounter ~ Options.
"""

m.addWorld(World("w0", {"push": 1, "refrain": 0, "tram_approaches": 1}))
m.addWorld(World("w1", {"push": 0, "refrain": 1, "tram_approaches": 1}))

m.setCurrentWorld("w0")
f = Prevents("push", Not("five_survive"))
b = m.models(f)
print(f, b)

"""
Double Effect Principle
"""
p, f, n = m.evaluate(DoubleEffectPrinciple, "w0", "w1")
print(p, f, n)

"""
Utilitarian Principle
"""
p, f, n = m.evaluate(UtilitarianPrinciple, "w0", "w1")
print(p, f, n)


"""
Utilitarian Principle
"""
p, f, n = m.evaluate(DoNoHarmPrinciple, "w0", "w1")
print(p, f, n)


"""
Pareto Principle
"""
p, f, n = m.evaluate(ParetoPrinciple, "w0", "w1")
print(p, f, n)
