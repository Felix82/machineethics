#from ethics.principles import KantianHumanityPrincipleReading1, KantianHumanityPrincipleOptimization1, KantianHumanityPrincipleReading2, KantianHumanityPrincipleOptimization2
from ethics.principles import KantianHumanityPrinciple
from ethics.semantics import CausalModel
#from ethics.language import Must, Or, PCauses, Causes, And, K, Not, May, Eq, U, Explains, Intervention, End, Affects, Means


model = CausalModel("./kantian_cases/flower_case.json", {"giveflowers":0, "refraining":1})
perm = model.evaluate(KantianHumanityPrinciple)
print(perm)

"""
print("#"*50)
print("Model Impermissible!")

print("Treated as Ends")
print("*"*50)
f1 = End("john")
f2 = End("sam")

print(f1, flower.models(f1), f2, flower.models(f2))

print("\n")

print("Treated as Means (Reading 1)")
print("*"*50)
f1 = Means("Reading-1", "john")
f2 = Means("Reading-1", "sam")

print(f1, flower.models(f1), f2, flower.models(f2))

print("\n")

print("Treated as Means (Reading 2)")
print("*"*50)
f1 = Means("Reading-2", "john")
f2 = Means("Reading-2", "sam")

print(f1, flower.models(f1), f2, flower.models(f2))

print("\n")

f = KantianHumanityPrincipleReading1(flower)
print("Permissibility:", f.permissible(), f.formulae)

f = KantianHumanityPrincipleReading2(flower)
print("Permissibility:", f.permissible(), f.formulae)

print("*"*50)

print("\n")

f = KantianHumanityPrincipleOptimization1(flower)
print("Permissibility:", f.permissible(), f.formulae)

f = KantianHumanityPrincipleOptimization2(flower)
print("Permissibility:", f.permissible(), f.formulae)



#####################
"""

"""
flower = CausalModel("./kantian_cases/flower_case_permissible.json", {"giveflowers":1, "refraining":0})
flower.setAlternatives(flower)


print("#"*50)
print("Model Permissible!")

print("Treated as Ends")
print("*"*50)
f1 = End("john")
f2 = End("sam")

print(f1, flower.models(f1), f2, flower.models(f2))

print("\n")

print("Treated as Means (Reading 1)")
print("*"*50)
f1 = Means("Reading-1", "john")
f2 = Means("Reading-1", "sam")

print(f1, flower.models(f1), f2, flower.models(f2))

print("\n")

print("Treated as Means (Reading 2)")
print("*"*50)
f1 = Means("Reading-2", "john")
f2 = Means("Reading-2", "sam")

print(f1, flower.models(f1), f2, flower.models(f2))

print("\n")

f = KantianHumanityPrincipleReading1(flower)
print("Permissibility:", f.permissible(), f.formulae)

f = KantianHumanityPrincipleReading2(flower)
print("Permissibility:", f.permissible(), f.formulae)

print("*"*50)

print("\n")

f = KantianHumanityPrincipleOptimization1(flower)
print("Permissibility:", f.permissible(), f.formulae)

f = KantianHumanityPrincipleOptimization2(flower)
print("Permissibility:", f.permissible(), f.formulae)

"""

