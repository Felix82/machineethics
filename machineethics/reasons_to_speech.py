import os

symbol_to_text = {"Not('fiftypeopledie')": "Fünfzig Menschen sterben nicht",
                  "Not('amoebe1dies')": "die eine Amöbe stirbt nicht",
                  "pull": "Ich betätige den Hebel"}

def reasons_to_speech(reasons, action, sequence = 0, voice = "Immanuel"):
    text = symbol_to_text[action]
    text += " denn: "
    for r in reasons:
        text += symbol_to_text[r]
    os.system("immanuel_talk " + str(sequence) + " " + voice + " \"" + text + "\"")
