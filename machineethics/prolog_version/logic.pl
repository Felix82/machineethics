% Helper 
notnot(not(F1), F1) :- !.
notnot(F1, not(F1)).

% Literals
satisfied(F, X) :-
    atom(F),
    (member(F, X) -> true;
     (member(not(F), X) -> false; 
      (mechanism(F, M) -> satisfied(M, X);
    situation(F, true)))).

satisfied(not(F), X) :-
    atom(F),
    (member(not(F), X) -> true;
     (member(F, X) -> false;
      (mechanism(F, M) -> not(satisfied(M, X));
    situation(F, false)))).

% Simple Propositional Logic
satisfied(and(F1, F2), X) :-
    satisfied(F1, X),
    satisfied(F2, X).
    
satisfied(or(F1, F2), X) :-
    satisfied(F1, X); 
    satisfied(F2, X).
    
satisfied(impl(F1, F2), X) :-
    satisfied(or(not(F1), F2), X).

satisfied(not(not(F)), X) :-
    satisfied(F, X).
    
satisfied(not(and(F1, F2)), X) :-
    satisfied(or(not(F1), not(F2)), X).
    
satisfied(not(or(F1, F2)), X) :-
    satisfied(and(not(F1), not(F2)), X).
    
satisfied(not(impl(F1, F2)), X) :-
    satisfied(and(F1, not(F2)), X).
    
satisfied(not(geq(F1, F2)), X) :-
    not(satisfied(geq(F1, F2), X)).
    
% Causation
satisfied(causes(F1, F2), X) :-
    satisfied(and(F1, F2), X),
    notnot(F1, F1ok),
    delete(X, F1ok, Xnew),
    append(Xnew, [F1ok], Xnew2),
    not(satisfied(F2, Xnew2)).

% Intention
satisfied(intended(F), _) :-
    action(A),
    situation(A, true),
    intended(A, F).

% Artihmetic formulae
satisfied(geq(F1, F2), _) :-
    \+integer(F1),
    \+integer(F2),
    sumutility(F1, U1),
    sumutility(F2, U2),
    U1 >= U2.
    
satisfied(geq(F1, F2), _) :-
    integer(F1),
    \+integer(F2),
    sumutility(F2, U2),
    F1 >= U2.
    
satisfied(geq(F1, F2), _) :-
    \+integer(F1),
    integer(F2),
    sumutility(F1, U1),
    U1 >= F2.

satisfied(geq(F1, F2), _) :-
    integer(F1),
    integer(F2),
    F1 >= F2.

satisfied(gt(F1, F2), X) :-
    satisfied(and(geq(F1, F2), not(geq(F2, F1))), X).

% Sum up Utilities
sumutility(and(F1, F2), U) :-
    sumutility(F1, U1),
    sumutility(F2, U2),
    U is U1 + U2.

sumutility(F, U) :-
    atom(F),
    utility(F, U).
    
sumutility(F, 0) :-
    atom(F),
    not(utility(F, _)).
    
sumutility(not(F), U) :-
    atom(F),
    utility(not(F), U).
    
sumutility(not(F), 0) :-
    atom(F),
    not(utility(not(F), _)).
