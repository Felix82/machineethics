% Examples

test_axioms :- axiom_k, axiom_functionality, axiom_composition, axiom_effectiveness, axiom_recursiveness.
axiom_k :- valid(impl(int([b], impl(c, d)), impl(int([b], c), int([b], d)))).
axiom_functionality :- valid(and(impl(int([b], c), not(int([b], not(c)))), impl(not(int([b], not(c))), int([b], c)))).
axiom_composition :- valid(impl(and(int([b], c), int([b], d)), int([b, c], d))).
axiom_effectiveness :- valid(int([b], b)).
axiom_recursiveness :- valid(impl(and(d, and(e, int([not(d)], not(e)))), not(and(d, and(e, int([not(e)], not(d))))))).

% Interfacing the tableaux prover

valid(F) :-
    \+ sat(not(F)). 

sat(Formula) :-
    nnf(Formula, F),
    expandTableaux([[[[], F]]], Tableaux),
    writeln(Tableaux),
    openBranchExists(Tableaux).

% Controlling the tableaux procedure

openBranchExists(T) :-
    member(B, T),
    \+ (member([S, F], B), member([S, not(F)], B)).


expandTableaux(T, Tnew) :-
    member(B, T),
    (
        (expandAnd(B, Bnew), writeln("AND"), writeln(B), writeln(Bnew),!);
        (expandBox(B, Bnew), writeln("BOX"), writeln(B), writeln(Bnew),!);   
        (expandLitImport(B, Bnew), writeln("IMPORT"), writeln(B), writeln(Bnew),!);
        (expandLitExport(B, Bnew), writeln("EXPORT"), writeln(B), writeln(Bnew),!);
        (expandLitRemoval(B, Bnew), writeln("REMOVAL"), writeln(B), writeln(Bnew),!)
    ),
    subtract(T, [B], T2),
    append([Bnew], T2, T3),
    expandTableaux(T3, Tnew).

expandTableaux(T, Tnew) :-
    member(B, T),
    expandOr(B, Bnew1, Bnew2), !,
    subtract(T, [B], T2),
    append([Bnew1], T2, T3),
    append([Bnew2], T3, T4),
    expandTableaux(T4, Tnew).
    
expandTableaux(T, T).

% The actual tableaux rules

expandAnd(B, Bnew) :-
    member([S, and(F1, F2)], B),
    (
        (
            \+ member([S, F1], B),
            append([[S, F1]], B, Bnew)
        );
        (
            \+ member([S, F2], B),
            append([[S, F2]], B, Bnew)
        )
    ).
    
expandOr(B, Bnew1, Bnew2) :-
    member([S, or(F1, F2)], B),
    \+ member([S, F1], B),
    \+ member([S, F2], B),
    append([[S, F1]], B, Bnew1),
    append([[S, F2]], B, Bnew2).
    
expandBox(B, Bnew) :-
    member([S, int(I, F)], B),
    append(I, S, Snew),
    \+ member([Snew, F], B),
    append([[Snew, F]], B, Bnew).

expandLitImport(B, Bnew) :-
    member([S, _], B),
    length(S, N),
    N > 0,
    member(F, S),
    \+ member([S, F], B),
    append([[S, F]], B, Bnew).
    
expandLitExport(B, Bnew) :-
    member([S1, F1], B),
    literal(F1),
    member([S2, F2], B),
    literal(F2),
    \+ setsDifferent(S1, S2),
    setAppend(S1, F1, S1new),
    \+ setMember([S1new, F2], B),
    append([[S1new, F2]], B, Bnew).

expandLitRemoval(B, Bnew) :-
    member([S1, F1], B),
    literal(F1),
    length(S1, N1),
    N1 > 1,
    member([S2, F2], B),
    literal(F2),
    length(S2, N2),
    N2 > 1,
    member(SB, S1),
    member(SB, S2),
    member(F2, S1),
    member(F1, S2),
    subtract(S1, [F2], S1new),
    \+ setMember([S1new, F1], B),
    append([[S1new, F1]], B, Bnew).
    
expandRecursiveness(B, Bnew) :-
    member([S, F1], B),
    literal(F1),
    member([S, F2], B),
    literal(F2),
    nnf(not(F1), F1x),
    append(S, [F1x], S1x),
    nnf(not(F2), F2x),
    member([S1x, F2x], B),
    append(S, [F2x], S2x),
    \+ setMember([S2x, F1], B),
    append([[S2x, F1]], B, Bnew).

% Helpful predicates

literal(F) :-
    atom(F).
literal(not(F)) :-
    atom(F).
    
setMember([S, F], B) :-
    member([Sx, Fx], B),
    \+ entriesDifferent([S, F], [Sx, Fx]).

entriesDifferent([S1, F1], [S2, F2]) :-
    F1 \= F2;
    setsDifferent(S1, S2).

setsDifferent(S1, S2) :-
    (member(E, S1),
    \+ member(E, S2));
    (member(E, S2),
    \+ member(E, S1)).
    
setAppend(S, E, S) :-
    member(E, S).
    
setAppend(S, E, [E | S]) :-
    \+ member(E, S).

list_to_and([L], [], L).
list_to_and([L], F, and(F, L)) :-
    \+ is_list(F).
list_to_and([L | R], F, and(A, L)) :-
    length(R, N),
    N > 0,
    list_to_and(R, F, A).
list_to_and(L, F) :-
    list_to_and(L, [], F).

% Transform formula into NNF to simplify rules

nnf(F, F) :-
    literal(F).  

nnf(not(and(F1, F2)), or(F1new, F2new)) :-
    nnf(not(F1), F1new),
    nnf(not(F2), F2new).
    
nnf(not(or(F1, F2)), and(F1new, F2new)) :-
    nnf(not(F1), F1new),
    nnf(not(F2), F2new).
    
nnf(not(impl(F1, F2)), and(F1new, F2new)) :-
    nnf(F1, F1new),
    nnf(not(F2), F2new).

nnf(not(int(I, F)), int(I, Fnew)) :- 
    nnf(not(F), Fnew).

nnf(not(not(F)), Fnew) :-
    nnf(F, Fnew).
    
nnf(and(F1, F2), and(F1new, F2new)) :-
    nnf(F1, F1new),
    nnf(F2, F2new).
    
nnf(or(F1, F2), or(F1new, F2new)) :-
    nnf(F1, F1new),
    nnf(F2, F2new).
    
nnf(impl(F1, F2), or(F1new, F2new)) :-
    nnf(not(F1), F1new),
    nnf(F2, F2new).
    
nnf(int(I, F), int(I, Fnew)) :-
    nnf(F, Fnew).
