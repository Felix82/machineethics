:-dynamic a/0, c1/0, c2/0.

action(a).
consequence(c1).
consequence(c2).

intended(a, a).
intended(a, c2).
intended(b, b).

a.

c1 :- satisfied(a).
c2 :- satisfied(a).

u(a, 0).
u(c1, -1).
u(c2, 5).
u(neg(c1), 1).
u(neg(c2), -5).
