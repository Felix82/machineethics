action(lying).
action(refraining).
consequence(false_bel).
consequence(exercises).
consequence(healthy).
background(motivated).

intended(refraining, refraining).
intended(lying, lying).
intended(lying, healthy).

mechanism(exercises, or(false_bel, motivated)).
mechanism(false_bel, lying).
mechanism(healthy, exercises).

utility(false_bel, -1).
utility(healthy, 10).
utility(not(healthy), -10).

situation(lying, true).
situation(refraining, false).
situation(motivated, false).
