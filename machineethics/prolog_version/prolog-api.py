from subprocess import PIPE, Popen
import json
import io
import sys

class CausalModel():
    """
    Representation of models in accordance
    to Definition 2.
    """
    def __init__(self, file, world):
        with io.open(file) as data_file:
            self.model = json.load(data_file)
            self.utilities = {str(k): v for k, v in self.model["utilities"].items()}
            self.actions = [str(a) for a in self.model["actions"]]
            self.description = str(self.model["description"])
            self.consequences = [str(c) for c in self.model["consequences"]]
            self.background = [str(b) for b in self.model["background"]]
            self.mechanisms = {str(k): str(v) for k, v in self.model["mechanisms"].items()}
            self.intentions = {str(k): list(map(str, v)) for k, v in self.model["intentions"].items()}
            self.world = world
            
    def translate_string(self, v):
        return v.replace("'", "").replace("Not", "not").lower()
        
case = sys.argv[1]
principle = sys.argv[2]
path = "../cases/"+case+".json"
model = CausalModel(path, {"motivated": 0, "lying": 1, "refraining": 0})

"""
prolog_file = ":-dynamic "

l = model.actions + model.consequences + model.background
for v, i in zip(l, range(len(l))):
    prolog_file += v+"/0"
    if i < len(l)-1:
        prolog_file += ","
    else:
        prolog_file += ".\n"
"""
prolog_file = ""

for v in model.actions:
    prolog_file += "action("+v+").\n"

for v in model.consequences:
    prolog_file += "consequence("+v+").\n"

for v in model.background:
    prolog_file += "background("+v+").\n"

prolog_file += "\n"

for k, v in model.intentions.items():
    for w in v:
        prolog_file += "intended("+k+", "+w+").\n"

prolog_file += "\n"

for k, v in model.mechanisms.items():
    prolog_file += "mechanism("+ k +", "+model.translate_string(v)+").\n"

prolog_file += "\n"

for k,v in model.utilities.items():
    prolog_file += "utility("+model.translate_string(k)+", "+str(v)+").\n"

prolog_file += "\n"

for v in model.world:
    if model.world[v] == 1:
        prolog_file += "situation("+v+", true).\n"

for v in model.world:
    if model.world[v] == 0:
        prolog_file += "situation("+v+", false).\n"

with open('./cases/'+case+'.pl', "w+") as f:
    f.write(prolog_file)

with Popen(['./judge', case, principle], stdout=PIPE) as proc:
    print(proc.stdout.read().decode('utf-8')[:-1])

