
actionNotBad :-
    action(A), satisfied(geq(A, 0), []).

noBadIntentions :-
    not((action(A), consequence(C), 
        satisfied(and(intended(C), and(gt(0, C), causes(A, C))), [])
        )).
    
oneGoodIntention :-
    action(A),
    consequence(C),
    satisfied(and(causes(A, C), gt(C, 0)), []).
    
noBadMeans :-  
    not((action(A), consequence(C1), consequence(C2),
        satisfied(and(gt(0, C1), and(gt(C2, 0), and(causes(A, C1), causes(C1, C2)))), [])
        )).

conjunction([], C, C).
conjunction([H|R], T, C) :-
    (T = true -> conjunction(R, H, C);
    conjunction(R, and(H, T), C)).

noNegativeTotalUtility :-
    action(A), findall(X, ((action(X);consequence(X)), satisfied(causes(A, X), [])), L), 
    conjunction(L, true, C),
    satisfied(geq(C, 0), []).
  
permitted :-
    actionNotBad,
    noBadIntentions,
    oneGoodIntention,
    noBadMeans,
    noNegativeTotalUtility.
