import math

class BayesianLearning:
    def __init__(self, hypotheses): 
        self.hypotheses = hypotheses # CausalModels, list
        
    def uniform_prior(self):
        p = 1 / len(self.hypotheses)
        for h in self.hypotheses:
            h.probability = p
        
    def learn(self, observation):
        if isinstance(observation, list):
            for o in observation:
                self.learn(o)
        elif isinstance(observation, tuple):
            p = self._getPosteriors(observation)
            try:
                normalizer = sum(p)
            except TypeError:
                print("Probabilities are not numbers. Did you forget to initialize a prior, e.g., you may want to run uniform_prior() first.")
            for h, u in zip(self.hypotheses, p):
                h.probability = u / normalizer
        
    def _getPosteriors(self, observation):
        post = []
        likelihoods = self._getLikelihoods(observation)
        for h, l in zip(self.hypotheses, likelihoods):
            p = l * h.probability
            post.append(p)
        return post
    
    def _getLikelihoods(self, o):
        likelihoods = []
        for h in self.hypotheses:
            d = self._getLikelihood(h, o)
            likelihoods.append(d)
        return likelihoods
    
    def _getlikelihood(self, h, o):
        """
        Computes some likelihood for observation
        o under hypothesis h.
        """
        pass
        
    def entropy(self):
        e = 0
        for h in self.hypotheses:
            try:
                e = e + h.probability * math.log(h.probability, 2)
            except TypeError:
                print("Probabilities are not numbers. Did you forget to initialize a prior, e.g., you may want to run uniform_prior() first.")
        e = -1 * e
        return e


class BayesianUtilityLearning(BayesianLearning):
    def __init__(self, hypotheses):
        super(BayesianUtilityLearning, self).__init__(hypotheses)
    
    def _getLikelihood(self, h, o): # assumes u to be in the form (consequence, utility)
        u_model = h.utilities[o[0]]
        u_obs = o[1]
        d = math.pow(u_obs - u_model, 2)
        d = math.exp(-1 * d)
        return d


if __name__ == "__main__":
    # Testing by example
    # Output
    def printModels():
        for m in models:
            print(m.utilities, m.probability)
            
    import sys
    sys.path.append("..")
    from ethics.semantics import CausalModel
    models = []
    x = 2
    u = range(-1*x, x)
    for i in u:
        for j in u:
            for k in u:
                for l in u:
                    for m in u:
                        for n in u:
                            m = CausalModel("../cases/lying-robot.json", {"lying": 1, "refraining": 0})
                            m.setUtilities(dict({"false_bel": i, "healthy": j, "Not('false_bel')": k, "Not('healthy')": l, "lying": m, "Not('lying')": n}))
                            models.append(m)
            
    print("Model built")
    
    #import ethics.tools as tools
    #tools.makeSetOfEpistemicAlternatives(*models)
    
    from ethics.language import U, Eq, And
    
    # Bayesian Learning
    bayes = BayesianUtilityLearning(models)
    print("Model Initialized")
    bayes.uniform_prior()
    print("Uniform Prior initialized")
    #printModels()
    print(bayes.entropy())
    print("-"*50)
    # Observation1
    o = ("healthy", 1)
    bayes.learn(o)
    #printModels()
    print(bayes.entropy())
    print("-"*50)
    # Observation2
    o = ("false_bel", -1)
    bayes.learn(o)
    #printModels()
    print(bayes.entropy())
    """
    print(models[0].degBelief(Eq(U("healthy"), 1)))
    print(models[0].degBelief(Eq(U("false_bel"), -1)))
    print(models[0].degBelief(And(Eq(U("healthy"), 1), Eq(U("false_bel"), -1))))
    """
