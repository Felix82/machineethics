#m is a causal model 
#Pro-Reasons: proReason(m)
#counterfactual reasons: minReason(m)
#sufficient reasons: sufficient(m)

# consequences of the alternative model
CONS_ALTERNATIVE = [] 

def setAlternativeConsequences(m):
    global CONS_ALTERNATIVE 
    CONS_ALTERNATIVE = getConsequences(m, False)

def getConsequences(m, considerAlternative = True):
    c = m.getAllConsequences()
    if considerAlternative:
        c = [x for x in c if x not in CONS_ALTERNATIVE]
    return list(map(str, c))
        
#return the reasons (compares all utilities)
def reason(m):
    l = getUtilities(m)[0]
    k = getUtilities(m)[1]
    explanation(m, l, k)

#get consequences for the chosen action
def getConsequences_katrin(m):
    l = []
    for i in range(0, len(m.consequences)):
        c = "Not('" + m.consequences[i] + "')"
        if m.action in str(m.mechanisms.get(m.consequences[i])):
            if type(m.mechanisms.get(m.consequences[i])) == str:
                l.append(m.consequences[i])
            if type(m.mechanisms.get(m.consequences[i])) == ethics.language.Not:
                l.append("Not('" + m.consequences[i] + "')")
            if type(m.mechanisms.get(m.consequences[i])) == ethics.language.Or:
                l.append(m.consequences[i])
            if type(m.mechanisms.get(m.consequences[i])) == ethics.language.And:
                l.append(m.consequences[i])
        else:
            if m.consequences[i] not in l and c not in l:
                l.append(c)
    return l

#get utilities to compare
def getUtilities(m):
    l = getConsequences(m)
    u = []
    w = []
    for i in range(0, len(m.consequences)):
        a = "Not('" + m.consequences[i] + "')"
        if m.consequences[i] in l:
            u.append(m.utilities[m.consequences[i]])
            w.append(m.utilities[a])
        else:
            u.append(m.utilities[a])
            w.append(m.utilities[m.consequences[i]])
    return u, w

#build a sentence with the utilities
def explanation(m, l, k):
    a = 0
    b = 0
    s = "I chose " + m.action + ", because "
    s1 = ""
    s2 = ""
    for i in range(0, len(l)):
        a = a + l[i]
        s1 = s1 + str(l[i])
        if i < len(l) - 1:
            s1 = s1 + " + "
    for j in range(0, len(k)):
        b = b + k[j]
        s2 = s2 + str(k[j])
        if j < len(k) - 1:
            s2 = s2 + " + "
    if a >= b:
        c = " >= "
    elif a <= b:
        c = " <= "
    elif a == b:
        c = " = "
    print(s + s1 + c + s2)

#return the power set of the consequences
def powerSet(m):
    import itertools
    c = getConsequences(m)
    p = [x for length in range(len(c) + 1) for x in itertools.combinations(c, length)]
    return p

#return the opposite consequences of a list c of consequences
def getOppositeConsequences(c):
    o = []
    for i in range(0, len(c)):
        if "Not" in c[i]:
            o.append(c[i].split("'")[1])
        else:
            o.append("Not('" + c[i] + "')")
    return o

#check if a list r is a minimum reason for m  
def isMinReason(m, r):
    c = getConsequences(m)
    oppositeC = getOppositeConsequences(c)
    oppositeR = getOppositeConsequences(r)
    sumC = 0
    sumOppositeC = 0
    for i in range(0, len(c)):
        sumC = sumC + m.utilities[c[i]]
        sumOppositeC = sumOppositeC + m.utilities[oppositeC[i]]
    sumR = sumC
    sumOppositeR = sumOppositeC
    for j in range(0, len(r)):
        sumR = sumR - m.utilities[r[j]]
        sumOppositeR = sumOppositeR - m.utilities[oppositeR[j]]
    if sumR <= sumOppositeR:
        return True
    else:
        return False

#return the minimum reason
def minReason(m):
    p = powerSet(m)
    minR = []
    for i in range(0, len(p)):
        helper = []
        for j in range(0, len(p[i])):
            helper.append(p[i][j])
        if isMinReason(m, helper) == True and helper != []:
            if minR != []:
                flag = True
                for k in range(0, len(helper)):
                    for h in range(0, len(minR)):
                        if helper[k] not in minR[h] and flag != False:
                            flag = True
                        else:
                            flag = False
                if flag == True:
                    minR.append(helper)
            else:
                minR.append(helper)
    return minR

#create a truth table, "u" is 0 and the consequences are 1
def truthTable(m):
    p = powerSet(m)
    matrix = [(["u"] * len(m.consequences)) for i in range(0, (2 ** len(m.consequences)))]
    for i in range(0, len(p)):
        for j in range(0, len(p[i])):
            for h in range(0, len(m.consequences)):
                if m.consequences[h] in p[i][j]:
                    matrix[i][h] = p[i][j]
    return matrix

#create the right side of the equation, the truth table is the left side
def opposite(m):
    matrixL = truthTable(m)
    k = 0
    lenML = len(matrixL)
    while k < lenML:
        if matrixL[k] == [0, ] * len(matrixL[k]):
            matrixL.remove(matrixL[k])
            k = k + 1
            lenML = len(matrixL)
        else:
            break
    matrixR = [[] for i in range(0, len(matrixL))]
    for i in range(0, len(matrixL)):
        l = matrixL[i]
        r = []
        for j in range(0, len(l)):
            value = l[j]
            if value != "u":
                if "Not" in value:
                    r.append(value.split("'")[1])
                else:
                    r.append("Not('" + value + "')")
            else:
                r.append("u")
        matrixR[i] = r
    return matrixL, matrixR

#check if the line of the truth table is true "t" or false "f"
def compareTruthTable(m):
    truth = opposite(m)[0]
    opp = opposite(m)[1]
    compare = []
    for i in range(0, len(truth)):
        sumTruth = 0
        sumOpp = 0
        for j in range(0, len(truth[i])):
            if truth[i][j] != "u":
                sumTruth = sumTruth + m.utilities[truth[i][j]]
                sumOpp = sumOpp + m.utilities[opp[i][j]]
        if sumTruth > sumOpp:
            if [truth[i], "t"] not in compare:
                compare.append([truth[i], "t"])
        else:
            if truth[i] not in compare:
                compare.append([truth[i], "f"])
    return compare

#check if a list r is a sufficient cause for the causal model m
def isSufficient(m, r):
    compare = compareTruthTable(m)
    c = getConsequences(m)
    flag = False
    for i in range(0, len(r)):
        for j in range(0, len(compare)):
            if r[i] in compare[j][0] and compare[j][1] == "f":
                if len(r) > 1:
                    for k in range(0, len(compare[j][0])):
                        if compare[j][0][k] not in r and compare[j][0][k] != "u":
                            return True
                        else:
                            return False
                else:
                    return False
            if r[i] in compare[j][0] and compare[j][1] == "t":
                flag = True
    if flag == True:
        return True
    else:
        return False

#return the sufficient cause
def sufficient(m):
    p = powerSet(m)
    suf = []
    for i in range(0, len(p)):
        helper = []
        for j in range(0, len(p[i])):
            helper.append(p[i][j])
        if helper != []:
            if isSufficient(m, helper) == True:
                suf.append(helper)
    return suf

#get the pro-reason
def proReason(m):
    c = getConsequences(m)
    u = getUtilities(m)[0]
    l = []
    for i in range(0, len(c)):
        if m.utilities[c[i]] > 0:
            l.append(c[i])
    return l
