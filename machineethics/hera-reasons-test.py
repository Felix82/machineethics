from ethics.semantics import CausalModel
import ethics.reasons as reasons
from reasons_to_speech import *

m1 = CausalModel("minimal_test.json", {"act": 1, "refrain":0})
m2 = CausalModel("minimal_test.json", {"act": 0, "refrain":1})
reasons.setAlternativeConsequences(m2)
print("-"*20)
print("Consequences of the situation and its alternative")
print("-"*20)
print("Not Act: " + str(reasons.CONS_ALTERNATIVE))
print("Act: " + str(reasons.getConsequences(m1)))
print("-"*20)
print("Pro Reasons: " + str(reasons.proReason(m1)))
print("MinReason: "+ str(reasons.minReason(m1)))
print("Sufficient: "+ str(reasons.sufficient(m1)))

#print(reasons.minReason(m1)[0])
#print(m1.action)



m1 = CausalModel("atom_bomb_trolley.json", {"pull": 1, "refrain":0})
m2 = CausalModel("atom_bomb_trolley.json", {"pull": 0, "refrain":1})
reasons.setAlternativeConsequences(m2)
print("-"*20)
print("Consequences of the situation and its alternative")
print("-"*20)
print("Pull: " + str(reasons.CONS_ALTERNATIVE))
print("Not Pull: " + str(reasons.getConsequences(m1)))
print("-"*20)
print("Pro Reasons: " + str(reasons.proReason(m1)))
print("MinReason: "+ str(reasons.minReason(m1)))
print("Sufficient: "+ str(reasons.sufficient(m1)))

#print(reasons.minReason(m1)[0])
#print(m1.action)

#reasons_to_speech(reasons.minReason(m1)[0], m1.action, 138)

