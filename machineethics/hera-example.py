from principles import DoubleEffectPrinciple, UtilitarianPrinciple, DoNoHarmPrinciple, ParetoPrinciple
from semantics import CausalModel, ActionTypeModel, World
from language import Must, Or, PCauses, Causes, And, K, Not, May, Eq, U, Explains
"""
import graphviz as gv

def drawGraph(m, f):
    g = gv.Digraph(format="jpg")
    for n in m.actions:
        g.node(n, shape="hexagon")
    for n in m.background:
        g.node(n, shape="square")
    for n in m.consequences:
        g.node(n, shape="ellipse")
    print(m.network)
    for p in m.network:
        for q in m.network[p]:
            g.edge(p, q)
    g.render(f)

drawGraph(CausalModel("./cases/rescue-robot.json"), "rescue-robot")
drawGraph(CausalModel("./cases/fatman-trolley-problem.json"), "fatman-trolley")
"""
"""
The Causal Network: Represents what the HERA knows
about actions and their consequences.
"""
forest = CausalModel("./cases/forest.json")
forest.setCurrentWorld(World("w0", {"md":1, "l":1}))

forest2 = CausalModel("./cases/forest.json")
forest2.setCurrentWorld(World("w1", {"md":0, "l":1}))

forest.setAlternatives(forest, forest2)
forest2.setAlternatives(forest, forest2)

fe = Explains("md", "ff")
print(fe, forest.models(fe))

f1 = Causes("md", "ff")
f2 = Causes("l", "ff")
f3 = Causes(And("md", "l"), "ff")
print(f1, forest.models(f1), f2, forest.models(f2), f3, forest.models(f3))
f4 = PCauses("md", "ff")
f5 = PCauses("l", "ff")
f6 = PCauses(And("md", "l"), "ff")
print(f4, forest.models(f4), f5, forest.models(f5), f6, forest.models(f6))

forest.setProbability(0.5)
forest.setAlternatives(forest)
print("Responsibility", forest.checker._computeDR(forest, "md", "ff"))
print("Blame", forest.checker._computeDB(forest, "md", "ff"))

m = CausalModel("./cases/rescue-robot.json")
m.setCurrentWorld(World("w0", {"a1": 1, "a2": 0, "a3": 0, "b1": 1}))

f1 = Eq(U("c1"), 10)
f2 = Eq(U(Not("c1")), -10)
f3 = Eq(U("a1"), 0)
f3 = Eq(U("a1"), 0)

print(m.models(f1), m.models(f2), m.models(f3), m.models(f4))

f = Causes("a1", "c1")
b = m.models(f)
print(f, b)

f = Causes("b1", "c1")
b = m.models(f)
print(f, b)

f = Causes(And("a1", "b1"), "c1")
b = m.models(f)
print(f, b)

f = Causes("c1", "c1")
b = m.models(f)
print(f, b)

f = Causes(And(And("a1", "b1"), "c1"), "c1")
b = m.models(f)
print(f, b)

print("-"*50)

m2 = CausalModel("./cases/rescue-robot.json")
m2.setCurrentWorld(World("w1", {"a1": 1, "a2": 0, "a3": 0, "b1": 0}))

"""
Test Epistemic Inference
"""
m.setAlternatives(m, m2)
m2.setAlternatives(m, m2)

f = K("b1")
b = m.models(f)
print(f, "is", b) # false
f = Not(K("b1"))
b = m.models(f)
print(f, "is", b) # True
f = K(Not("b1"))
b = m.models(f)
print(f, "is", b) # False

"""
Possible Worlds representing
the situation the HERA can
encounter ~ Options.
"""
m.resetWorlds()
m.addWorld(World("w0", {"a1": 1, "a2": 0, "a3": 0, "b1": 1}))
m.addWorld(World("w1", {"a1": 0, "a2": 1, "a3": 0, "b1": 1}))
m.addWorld(World("w2", {"a1": 0, "a2": 0, "a3": 1, "b1": 1}))

"""
Double Effect Principle
"""
p, f, n = m.evaluate(DoubleEffectPrinciple, "w0", "w1", "w2")
print("PDE", p, f, n)

"""
Utilitarian Principle
"""
p, f, n = m.evaluate(UtilitarianPrinciple, "w0", "w1", "w2")
print("Utilitarianism", p, f, n)

"""
DoNoHarm Principle
"""
p, f, n = m.evaluate(DoNoHarmPrinciple, "w0", "w1", "w2")
print("Do no harm", p, f, n)

"""
Pareto Principle
"""
p, f, n = m.evaluate(ParetoPrinciple, "w0", "w1", "w2")
print("Pareto", p, f, n)

print("-"*50)

"""
Now let's see what happens if nobody is in danger:
"""
m.resetWorlds()
m.addWorld(World("w3", {"a1": 1, "a2": 0, "a3": 0, "b1": 0}))
m.addWorld(World("w4", {"a1": 0, "a2": 1, "a3": 0, "b1": 0}))
m.addWorld(World("w5", {"a1": 0, "a2": 0, "a3": 1, "b1": 0}))

"""
Double Effect Principle
"""
p, f, n = m.evaluate(DoubleEffectPrinciple, "w3", "w4", "w5")
print("PDE", p, f, n)

"""
Utilitarian Principle
"""
p, f, n = m.evaluate(UtilitarianPrinciple, "w3", "w4", "w5")
print("Utilitarianism", p, f, n)


"""
DoNoHarm Principle
"""
p, f, n = m.evaluate(DoNoHarmPrinciple, "w3", "w4", "w5")
print("Do no harm", p, f, n)

"""
Pareto Principle
"""
p, f, n = m.evaluate(ParetoPrinciple, "w3", "w4", "w5")
print("Pareto", p, f, n)

print("-"*50)

"""
Action Type Deontic Logic
"""
m = ActionTypeModel({"A1": {"a1"}, "A2": {"a2"}, "A3": set({})}, {"a1", "a2"})
f = May("A3")
r = m.models(f)
print(str(f)+" is "+str(r))

m2 = ActionTypeModel({"A1": {"a1"}, "A2": set({}), "A3": {"a3"}}, {"a1", "a3"})
f = May("A3")
r = m2.models(f)
print(str(f)+" is "+str(r))

"""
Epistemic Stuff
"""
m.setAlternatives(m, m2)
m2.setAlternatives(m, m2)

f = K(May("A3"))
r = m.models(f)
print(str(f)+" is "+str(r))

f = K(Not(May("A3")))
r = m.models(f)
print(str(f)+" is "+str(r))

f = Not(K(May("A3")))
r = m.models(f)
print(str(f)+" is "+str(r))
