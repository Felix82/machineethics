from ethics.moralplans import Situation, KantianHumanity, DoNoHarm, DoNoInstrumentalHarm, Utilitarianism, Deontology, DoubleEffectPrinciple, AvoidAnyHarm, AvoidAvoidableHarm

print("*"*50)
sit = Situation("plan-cases/flowers.json")

print("Intial State: "+ str(sit.init))
print("Plan: "+ str(sit.plan))
print("Final State: "+ str(sit.getAllConsequences()))

perm = sit.evaluate(Deontology)
print("Deontology: ", perm)

perm = sit.evaluate(KantianHumanity)
print("Kantian: ", perm)

perm = sit.evaluate(DoNoHarm)
print("DoNoHarm: ", perm)

perm = sit.evaluate(DoNoInstrumentalHarm)
print("DoNoInstrumentalHarm: ", perm)

perm = sit.evaluate(Utilitarianism)
print("Utilitarianism: ", perm)

perm = sit.evaluate(DoubleEffectPrinciple)
print("DoubleEffectPrinciple: ", perm)


p = sit.generatePlan()
if p:
    print("Planned: ", p.plan)
else:
    print("No Plan found")

print("*"*50)

sit = Situation("plan-cases/trolley-50.json")

print("Intial State: "+ str(sit.init))
print("Plan: "+ str(sit.plan))
print("Final State: "+ str(sit.getAllConsequences()))


perm = sit.evaluate(Deontology)
print("Deontology: ", perm)

perm = sit.evaluate(KantianHumanity)
print("Kantian: ", perm)

perm = sit.evaluate(DoNoHarm)
print("DoNoHarm: ", perm)

perm = sit.evaluate(DoNoInstrumentalHarm)
print("DoNoInstrumentalHarm: ", perm)

perm = sit.evaluate(Utilitarianism)
print("Utilitarianism: ", perm)

perm = sit.evaluate(DoubleEffectPrinciple)
print("DoubleEffectPrinciple: ", perm)


p = sit.generatePlan(principle = DoNoHarm)
if p:
    print("Planned: ", p.plan)
else:
    print("No Plan found")


print("*"*50)

sit = Situation("plan-cases/coal-dilemma.json")

print("Intial State: "+ str(sit.init))
print("Plan: "+ str(sit.plan))
print("Final State: "+ str(sit.getAllConsequences()))


perm = sit.evaluate(Deontology)
print("Deontology: ", perm)

perm = sit.evaluate(KantianHumanity)
print("Kantian: ", perm)

perm = sit.evaluate(DoNoHarm)
print("DoNoHarm: ", perm)

perm = sit.evaluate(DoNoInstrumentalHarm)
print("DoNoInstrumentalHarm: ", perm)

perm = sit.evaluate(Utilitarianism)
print("Utilitarianism: ", perm)

perm = sit.evaluate(DoubleEffectPrinciple)
print("DoubleEffectPrinciple: ", perm)


p = sit.generatePlan()
if p:
    print("Planned: ", p.plan)
else:
    print("No Plan found")


print("*"*50)

sit = Situation("plan-cases/coal-dilemma.json")

print("Intial State: "+ str(sit.init))
print("Plan: "+ str(sit.plan))
print("Final State: "+ str(sit.getAllConsequences()))


perm = sit.evaluate(Deontology)
print("Deontology: ", perm)

perm = sit.evaluate(KantianHumanity, 1)
print("Kantian 1: ", perm)

perm = sit.evaluate(KantianHumanity, 2)
print("Kantian 2: ", perm)

perm = sit.evaluate(DoNoHarm)
print("DoNoHarm: ", perm)

perm = sit.evaluate(DoNoInstrumentalHarm)
print("DoNoInstrumentalHarm: ", perm)

perm = sit.evaluate(Utilitarianism)
print("Utilitarianism: ", perm)

perm = sit.evaluate(DoubleEffectPrinciple)
print("DoubleEffectPrinciple: ", perm)

p = sit.generatePlan(principle = AvoidAnyHarm)
if p:
    print("Planned: ", p.plan)
else:
    print("No Plan found")

sit.creativeAlternatives += [Situation("plan-cases/coal-dilemma-creative1.json")]
a = sit.makeMoralSuggestion(AvoidAvoidableHarm)
print(a, a.plan)
