from ethics.principles import DoubleEffectPrinciple, UtilitarianPrinciple, DoNoHarmPrinciple, ParetoPrinciple, MinimizeHarmPrinciple, MinimaxHarmPrinciple, DeontologicalPrinciple
from ethics.semantics import CausalModel, ActionTypeModel
from ethics.language import Must, Or, PCauses, Causes, And, K, Not, May, Eq, U, Explains, Intervention, Forall, Impl, Consequence, Exists, Gt


forest = CausalModel("./cases/forest.json", {"md":1, "l":1})

f = "test"
print(f, forest.models(f))

f = Forall('x', Impl(Consequence('x'), 'x'))
print(f)
print(forest.models(f))

f = Exists('x', And(Consequence('x'), Not('x')))
print(f, forest.models(f))

forest2 = CausalModel("./cases/forest.json", {"md":0, "l":1})

forest.setAlternatives(forest, forest2)
forest2.setAlternatives(forest, forest2)

s = And("md", "l")
print(s, forest.models(Intervention(Not("l"), s)))

fe = Explains("md", "ff")
print(fe, forest.models(fe))
f1 = Causes("md", "ff")
f2 = Causes("l", "ff")
f3 = Causes(And("md", "l"), "ff")
print(f1, forest.models(f1), f2, forest.models(f2), f3, forest.models(f3))
f4 = PCauses("md", "ff")
f5 = PCauses("l", "ff")
f6 = PCauses(And("md", "l"), "ff")
print(f4, forest.models(f4), f5, forest.models(f5), f6, forest.models(f6))

forest.setProbability(0.5)
forest.setAlternatives(forest)
print("Responsibility", forest.checker._computeDR(forest, "md", "ff"))
print("Blame", forest.checker._computeDB(forest, "md", "ff"))

m = CausalModel("./cases/rescue-robot.json", {"a1": 1, "a2": 0, "a3": 0, "b1": 1})

f1 = Eq(U("c1"), 10)
f2 = Eq(U(Not("c1")), -10)
f3 = Eq(U("a1"), 0)
f3 = Eq(U("a1"), 0)

print(m.models(f1), m.models(f2), m.models(f3), m.models(f4))

f = Causes("a1", "c1")
b = m.models(f)
print(f, b)

f = Causes("b1", "c1")
b = m.models(f)
print(f, b)

f = Causes(And("a1", "b1"), "c1")
b = m.models(f)
print(f, b)

f = Causes("c1", "c1")
b = m.models(f)
print(f, b)

f = Causes(And(And("a1", "b1"), "c1"), "c1")
b = m.models(f)
print(f, b)

print("-"*50)


m1 = CausalModel("./cases/rescue-robot.json", {"a1": 1, "a2": 0, "a3": 0, "b1": 1})
m2 = CausalModel("./cases/rescue-robot.json", {"a1": 0, "a2": 1, "a3": 0, "b1": 1})
m3 = CausalModel("./cases/rescue-robot.json", {"a1": 0, "a2": 0, "a3": 1, "b1": 1})

m1.setAlternatives(m1, m2, m3)
m2.setAlternatives(m1, m2, m3)
m3.setAlternatives(m1, m2, m3)

"""
Double Effect Principle
"""
b1 = m1.evaluate(DoubleEffectPrinciple)
b2 = m2.evaluate(DoubleEffectPrinciple)
b3 = m3.evaluate(DoubleEffectPrinciple)
print("PDE", b1, b2, b3)

"""
Utilitarian Principle
"""
b1 = m1.evaluate(UtilitarianPrinciple)
b2 = m2.evaluate(UtilitarianPrinciple)
b3 = m3.evaluate(UtilitarianPrinciple)
print("Utilitarianism", b1, b2, b3)


"""
DoNoHarm Principle
"""
b1 = m1.evaluate(DoNoHarmPrinciple)
b2 = m2.evaluate(DoNoHarmPrinciple)
b3 = m3.evaluate(DoNoHarmPrinciple)
print("DoNoHarmPrinciple", b1, b2, b3)


"""
MinimizeHarm Principle
"""
b1 = m1.evaluate(MinimizeHarmPrinciple)
b2 = m2.evaluate(MinimizeHarmPrinciple)
b3 = m3.evaluate(MinimizeHarmPrinciple)
print("MinimizeHarmPrinciple", b1, b2, b3)


"""
MinimaxHarmPrinciple Principle
"""
b1 = m1.evaluate(MinimaxHarmPrinciple)
b2 = m2.evaluate(MinimaxHarmPrinciple)
b3 = m3.evaluate(MinimaxHarmPrinciple)
print("MinimaxHarmPrinciple", b1, b2, b3)

"""
Pareto Principle
"""
b1 = m1.evaluate(ParetoPrinciple)
b2 = m2.evaluate(ParetoPrinciple)
b3 = m3.evaluate(ParetoPrinciple)
print("ParetoPrinciple", b1, b2, b3)


"""
DeontologicalPrinciple Principle
"""
b1 = m1.evaluate(DeontologicalPrinciple)
b2 = m2.evaluate(DeontologicalPrinciple)
b3 = m3.evaluate(DeontologicalPrinciple)
print("DeontologicalPrinciple", b1, b2, b3)


print("-"*50)



m1 = CausalModel("./cases/rescue-robot.json", {"a1": 1, "a2": 0, "a3": 0, "b1": 0})
m2 = CausalModel("./cases/rescue-robot.json", {"a1": 0, "a2": 1, "a3": 0, "b1": 0})
m3 = CausalModel("./cases/rescue-robot.json", {"a1": 0, "a2": 0, "a3": 1, "b1": 0})

m1.setAlternatives(m1, m2, m3)
m2.setAlternatives(m1, m2, m3)
m3.setAlternatives(m1, m2, m3)

"""
Double Effect Principle
"""
b1 = m1.evaluate(DoubleEffectPrinciple)
b2 = m2.evaluate(DoubleEffectPrinciple)
b3 = m3.evaluate(DoubleEffectPrinciple)
print("PDE", b1, b2, b3)

"""
Utilitarian Principle
"""
b1 = m1.evaluate(UtilitarianPrinciple)
b2 = m2.evaluate(UtilitarianPrinciple)
b3 = m3.evaluate(UtilitarianPrinciple)
print("Utilitarianism", b1, b2, b3)


"""
DoNoHarm Principle
"""
b1 = m1.evaluate(DoNoHarmPrinciple)
b2 = m2.evaluate(DoNoHarmPrinciple)
b3 = m3.evaluate(DoNoHarmPrinciple)
print("DoNoHarmPrinciple", b1, b2, b3)


"""
MinimizeHarm Principle
"""
b1 = m1.evaluate(MinimizeHarmPrinciple)
b2 = m2.evaluate(MinimizeHarmPrinciple)
b3 = m3.evaluate(MinimizeHarmPrinciple)
print("MinimizeHarmPrinciple", b1, b2, b3)

"""
MinimaxHarmPrinciple Principle
"""
b1 = m1.evaluate(MinimaxHarmPrinciple)
b2 = m2.evaluate(MinimaxHarmPrinciple)
b3 = m3.evaluate(MinimaxHarmPrinciple)
print("MinimaxHarmPrinciple", b1, b2, b3)

"""
Pareto Principle
"""
b1 = m1.evaluate(ParetoPrinciple)
b2 = m2.evaluate(ParetoPrinciple)
b3 = m3.evaluate(ParetoPrinciple)
print("ParetoPrinciple", b1, b2, b3)


"""
DeontologicalPrinciple Principle
"""
b1 = m1.evaluate(DeontologicalPrinciple)
b2 = m2.evaluate(DeontologicalPrinciple)
b3 = m3.evaluate(DeontologicalPrinciple)
print("DeontologicalPrinciple", b1, b2, b3)


print("-"*50)


"""
Action Type Deontic Logic
"""
m = ActionTypeModel({"A1": {"a1"}, "A2": {"a2"}, "A3": set({})}, {"a1", "a2"})
f = May("A3")
r = m.models(f)
print(str(f)+" is "+str(r))

m2 = ActionTypeModel({"A1": {"a1"}, "A2": set({}), "A3": {"a3"}}, {"a1", "a3"})
f = May("A3")
r = m2.models(f)
print(str(f)+" is "+str(r))

"""
Epistemic Stuff
"""
m.setEpistemicAlternatives(m, m2)
m2.setEpistemicAlternatives(m, m2)

f = K(May("A3"))
r = m.models(f)
print(str(f)+" is "+str(r))

f = K(May("A1"))
r = m.models(f)
print(str(f)+" is "+str(r))

f = K(Not(May("A3")))
r = m.models(f)
print(str(f)+" is "+str(r))

f = Not(K(May("A3")))
r = m.models(f)
print(str(f)+" is "+str(r))

print("-"*50)

m1 = CausalModel("./cases/rescue-robot.json", {"a1": 1, "a2": 0, "a3": 0, "b1": 1})
m2 = CausalModel("./cases/rescue-robot.json", {"a1": 0, "a2": 1, "a3": 0, "b1": 1})
m3 = CausalModel("./cases/rescue-robot.json", {"a1": 0, "a2": 0, "a3": 1, "b1": 1})

m4 = CausalModel("./cases/rescue-robot.json", {"a1": 1, "a2": 0, "a3": 0, "b1": 0})
m5 = CausalModel("./cases/rescue-robot.json", {"a1": 0, "a2": 1, "a3": 0, "b1": 0})
m6 = CausalModel("./cases/rescue-robot.json", {"a1": 0, "a2": 0, "a3": 1, "b1": 0})


import ethics.tools as tools
tools.makeSetOfAlternatives(m1, m2, m3)
tools.makeSetOfAlternatives(m4, m5, m6)

tools.makeSetOfEpistemicAlternatives(m1, m2, m3, m4, m5, m6)

dp1 = m3.degPerm(UtilitarianPrinciple)
print(dp1)

print("-"*50)
