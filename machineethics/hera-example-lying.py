from principles import DoubleEffectPrinciple, UtilitarianPrinciple, DoNoHarmPrinciple, ParetoPrinciple
from semantics import CausalModel, ActionTypeModel, World
from language import Must, Or, Causes, And, Intervention

import graphviz as gv

def drawGraph(m, f):
    g = gv.Digraph(format="jpg")
    for n in m.actions:
        g.node(n, shape="hexagon")
    for n in m.background:
        g.node(n, shape="square")
    for n in m.consequences:
        g.node(n, shape="ellipse")
    print(m.network)
    for p in m.network:
        for q in m.network[p]:
            g.edge(p, q)
    g.render(f)
    
"""
The Causal Network: Represents what the HERA knows
about actions and their consequences.
"""
m = CausalModel("./cases/lying-robot.json")
drawGraph(m, "lying-robot")
m.setCurrentWorld(World("w0", {"lying": 1, "refraining": 0, "motivated": 0}))
f = Causes("lying", "exercises")
b = m.models(f)
print(f, b)

f = Intervention("motivated", Causes("lying", "exercises"))
b = m.models(f)
print(f, b)

"""
Possible Worlds representing
the situation the HERA can
encounter ~ Options.
"""

m.resetWorlds()
m.addWorld(World("w0", {"lying": 1, "refraining": 0, "motivated": 0}))
m.addWorld(World("w1", {"lying": 0, "refraining": 1, "motivated": 0}))

"""
Double Effect Principle
"""
p, f, n = m.evaluate(DoubleEffectPrinciple, "w0", "w1")
print(p, f, n)

"""
Utilitarian Principle
"""
p, f, n = m.evaluate(UtilitarianPrinciple, "w0", "w1")
print(p, f, n)

"""
DoNoHarm Principle
"""
p, f, n = m.evaluate(DoNoHarmPrinciple, "w0", "w1")
print(p, f, n)

"""
Pareto Principle
"""
p, f, n = m.evaluate(ParetoPrinciple, "w0", "w1")
print(p, f, n)
